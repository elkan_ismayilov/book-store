package az.ingress.booklibrary.service;

import az.ingress.booklibrary.model.library.LibraryRequest;
import az.ingress.booklibrary.model.library.LibraryResponse;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:34
 */
public interface LibraryService {
    LibraryResponse createLibrary(LibraryRequest libraryRequest);
    List<LibraryResponse> getLibraries();
}
