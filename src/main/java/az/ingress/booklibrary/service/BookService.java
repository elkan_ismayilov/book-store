package az.ingress.booklibrary.service;

import az.ingress.booklibrary.model.book.BookRequest;
import az.ingress.booklibrary.model.book.BookResponse;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:33
 */
public interface BookService {
    BookResponse create(BookRequest bookRequest);
    List<BookResponse> getBooks();
}
