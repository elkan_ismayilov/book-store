package az.ingress.booklibrary.service.impl;

import az.ingress.booklibrary.entity.Book;
import az.ingress.booklibrary.entity.Library;
import az.ingress.booklibrary.exception.NotFoundException;
import az.ingress.booklibrary.model.book.BookRequest;
import az.ingress.booklibrary.model.book.BookResponse;
import az.ingress.booklibrary.repository.BookRepository;
import az.ingress.booklibrary.repository.LibraryRepository;
import az.ingress.booklibrary.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:34
 */
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final LibraryRepository libraryRepository;
    @Override
    public BookResponse create(BookRequest bookRequest) {
        Book book = Book.builder()
                .name(bookRequest.getName())
                .author(bookRequest.getAuthor())
                .library(getLibrary(bookRequest.getLibraryId()))
                .build();
        Book saveBook = bookRepository.save(book);
        return BookResponse.builder()
                .id(saveBook.getId())
                .name(saveBook.getName())
                .author(saveBook.getAuthor())
                .build();
    }

    @Override
    public List<BookResponse> getBooks() {
        List<Book> books = bookRepository.findAll();
        return books.stream()
                .map(book -> BookResponse.builder()
                        .id(book.getId())
                        .name(book.getName())
                        .author(book.getAuthor())
                        .build())
                .toList();
    }

    private Library getLibrary(Long libraryId) {
        return libraryRepository.findById(libraryId)
                .orElseThrow(() -> new NotFoundException("Library not found", "LIBRARY_NOT_FOUND"));
    }
}
