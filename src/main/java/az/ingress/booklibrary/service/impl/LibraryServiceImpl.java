package az.ingress.booklibrary.service.impl;

import az.ingress.booklibrary.entity.Book;
import az.ingress.booklibrary.entity.Library;
import az.ingress.booklibrary.model.book.BookResponse;
import az.ingress.booklibrary.model.library.LibraryRequest;
import az.ingress.booklibrary.model.library.LibraryResponse;
import az.ingress.booklibrary.repository.BookRepository;
import az.ingress.booklibrary.repository.LibraryRepository;
import az.ingress.booklibrary.service.LibraryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:46
 */
@Service
@AllArgsConstructor
public class LibraryServiceImpl implements LibraryService {
    private final LibraryRepository libraryRepository;
    private final BookRepository bookRepository;
    @Override
    public LibraryResponse createLibrary(LibraryRequest libraryRequest) {
        Library library = Library.builder()
                .name(libraryRequest.getName())
                .address(libraryRequest.getAddress())
                .build();

        Library saveLibrary = libraryRepository.save(library);

        return LibraryResponse.builder()
                .id(saveLibrary.getId())
                .name(saveLibrary.getName())
                .address(saveLibrary.getAddress())
                .books(getBookResponses(saveLibrary.getId()))
                .build();
    }

    @Override
    public List<LibraryResponse> getLibraries() {
        List<Library> libraries = libraryRepository.findAll();
        return libraries.stream()
                .map(library -> LibraryResponse.builder()
                        .id(library.getId())
                        .name(library.getName())
                        .address(library.getAddress())
                        .books(getBookResponses(library.getId()))
                        .build())
                .toList();
    }

    private List<BookResponse> getBookResponses(Long libraryId) {
        List<Book> books = bookRepository.findAllByLibraryId(libraryId);
        return books.stream()
                .map(book -> BookResponse.builder()
                        .id(book.getId())
                        .name(book.getName())
                        .author(book.getAuthor())
                        .build())
                .toList();
    }
}
