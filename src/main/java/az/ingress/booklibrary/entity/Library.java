package az.ingress.booklibrary.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:20
 */
@Data
@Entity
@Table(name = "library")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    @OneToMany(mappedBy = "library", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Book> books;
}
