package az.ingress.booklibrary.repository;

import az.ingress.booklibrary.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:33
 */
public interface LibraryRepository extends JpaRepository<Library, Long> {
}
