package az.ingress.booklibrary.repository;

import az.ingress.booklibrary.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:32
 */
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByLibraryId(Long libraryId);
}
