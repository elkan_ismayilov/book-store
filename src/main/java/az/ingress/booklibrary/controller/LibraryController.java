package az.ingress.booklibrary.controller;

import az.ingress.booklibrary.model.library.LibraryRequest;
import az.ingress.booklibrary.model.library.LibraryResponse;
import az.ingress.booklibrary.service.LibraryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 21:47
 */
@RestController
@RequestMapping("/api/library")
@RequiredArgsConstructor
public class LibraryController {
    private final LibraryService libraryService;

    @PostMapping
    public ResponseEntity<LibraryResponse> createLibrary(@RequestBody LibraryRequest libraryRequest) {
        return ResponseEntity.ok(libraryService.createLibrary(libraryRequest));
    }

    @GetMapping
    public ResponseEntity<List<LibraryResponse>> getLibraries() {
        return ResponseEntity.ok(libraryService.getLibraries());
    }
}
