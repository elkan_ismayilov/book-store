package az.ingress.booklibrary.controller;

import az.ingress.booklibrary.model.book.BookRequest;
import az.ingress.booklibrary.model.book.BookResponse;
import az.ingress.booklibrary.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 21:29
 */
@RestController
@RequestMapping("/api/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping
    public ResponseEntity<BookResponse> create(@RequestBody BookRequest bookRequest) {
        return ResponseEntity.ok(bookService.create(bookRequest));
    }

    @GetMapping
    public ResponseEntity<List<BookResponse>> getBooks() {
        return ResponseEntity.ok(bookService.getBooks());
    }
}
