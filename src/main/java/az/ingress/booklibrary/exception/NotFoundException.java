package az.ingress.booklibrary.exception;

import lombok.*;

/**
 * book-library
 * Elkhan
 * 01.02.2024 21:33
 */
@Getter
public class NotFoundException extends RuntimeException {
    private final String message;
    private final String code;
    public NotFoundException(String message, String code) {
        this.message = message;
        this.code = code;
    }

}
