package az.ingress.booklibrary.model.error;

import lombok.Builder;
import lombok.Data;

/**
 * book-library
 * Elkhan
 * 01.02.2024 21:35
 */
@Data
@Builder
public class ErrorResponse {
    private String message;
    private String code;

    public ErrorResponse(String message, String code) {
        this.message = message;
        this.code = code;
    }
}
