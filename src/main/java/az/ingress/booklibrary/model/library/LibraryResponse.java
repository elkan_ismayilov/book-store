package az.ingress.booklibrary.model.library;

import az.ingress.booklibrary.model.book.BookResponse;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:25
 */
@Data
@Builder
public class LibraryResponse {
    private Long id;
    private String name;
    private String address;
    private List<BookResponse> books;
}
