package az.ingress.booklibrary.model.library;

import lombok.Builder;
import lombok.Data;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:25
 */
@Data
@Builder
public class LibraryRequest {
    private String name;
    private String address;
}
