package az.ingress.booklibrary.model.book;

import lombok.Builder;
import lombok.Data;

/**
 * book-library
 * Elkhan
 * 01.02.2024 20:24
 */
@Data
@Builder
public class BookRequest {
    private String name;
    private String author;
    private Long libraryId;
}
