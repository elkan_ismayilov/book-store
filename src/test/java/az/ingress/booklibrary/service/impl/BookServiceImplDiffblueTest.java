package az.ingress.booklibrary.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.booklibrary.entity.Book;
import az.ingress.booklibrary.entity.Library;
import az.ingress.booklibrary.exception.NotFoundException;
import az.ingress.booklibrary.model.book.BookRequest;
import az.ingress.booklibrary.model.book.BookResponse;
import az.ingress.booklibrary.repository.BookRepository;
import az.ingress.booklibrary.repository.LibraryRepository;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {BookServiceImpl.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class BookServiceImplDiffblueTest {
    @MockBean
    private BookRepository bookRepository;

    @Autowired
    private BookServiceImpl bookServiceImpl;

    @MockBean
    private LibraryRepository libraryRepository;

    /**
     * Method under test: {@link BookServiceImpl#create(BookRequest)}
     */
    @Test
    void testCreate() {
        // Arrange
        Library library = new Library();
        library.setAddress("42 Main St");
        library.setBooks(new ArrayList<>());
        library.setId(1L);
        library.setName("Name");

        Book book = new Book();
        book.setAuthor("JaneDoe");
        book.setId(1L);
        book.setLibrary(library);
        book.setName("Name");
        when(bookRepository.save(Mockito.<Book>any())).thenReturn(book);

        Library library2 = new Library();
        library2.setAddress("42 Main St");
        library2.setBooks(new ArrayList<>());
        library2.setId(1L);
        library2.setName("Name");
        Optional<Library> ofResult = Optional.of(library2);
        when(libraryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        BookRequest bookRequest = BookRequest.builder().author("JaneDoe").libraryId(1L).name("Name").build();

        // Act
        BookResponse actualCreateResult = bookServiceImpl.create(bookRequest);

        // Assert
        verify(libraryRepository).findById(Mockito.<Long>any());
        verify(bookRepository).save(Mockito.<Book>any());
        assertEquals("JaneDoe", actualCreateResult.getAuthor());
        assertEquals("Name", actualCreateResult.getName());
        assertEquals(1L, actualCreateResult.getId().longValue());
    }

    /**
     * Method under test: {@link BookServiceImpl#create(BookRequest)}
     */
    @Test
    void testCreate2() {
        // Arrange
        when(bookRepository.save(Mockito.<Book>any())).thenThrow(new NotFoundException("An error occurred", "Code"));

        Library library = new Library();
        library.setAddress("42 Main St");
        library.setBooks(new ArrayList<>());
        library.setId(1L);
        library.setName("Name");
        Optional<Library> ofResult = Optional.of(library);
        when(libraryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        BookRequest bookRequest = BookRequest.builder().author("JaneDoe").libraryId(1L).name("Name").build();

        // Act and Assert
        assertThrows(NotFoundException.class, () -> bookServiceImpl.create(bookRequest));
        verify(libraryRepository).findById(Mockito.<Long>any());
        verify(bookRepository).save(Mockito.<Book>any());
    }

    /**
     * Method under test: {@link BookServiceImpl#create(BookRequest)}
     */
    @Test
    void testCreate3() {
        // Arrange
        Library library = new Library();
        library.setAddress("42 Main St");
        library.setBooks(new ArrayList<>());
        library.setId(1L);
        library.setName("Name");
        Book book = mock(Book.class);
        when(book.getId()).thenReturn(1L);
        when(book.getAuthor()).thenReturn("JaneDoe");
        when(book.getName()).thenReturn("Name");
        doNothing().when(book).setAuthor(Mockito.<String>any());
        doNothing().when(book).setId(Mockito.<Long>any());
        doNothing().when(book).setLibrary(Mockito.<Library>any());
        doNothing().when(book).setName(Mockito.<String>any());
        book.setAuthor("JaneDoe");
        book.setId(1L);
        book.setLibrary(library);
        book.setName("Name");
        when(bookRepository.save(Mockito.<Book>any())).thenReturn(book);

        Library library2 = new Library();
        library2.setAddress("42 Main St");
        library2.setBooks(new ArrayList<>());
        library2.setId(1L);
        library2.setName("Name");
        Optional<Library> ofResult = Optional.of(library2);
        when(libraryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        BookRequest bookRequest = BookRequest.builder().author("JaneDoe").libraryId(1L).name("Name").build();

        // Act
        BookResponse actualCreateResult = bookServiceImpl.create(bookRequest);

        // Assert
        verify(book).getAuthor();
        verify(book).getId();
        verify(book).getName();
        verify(book).setAuthor(eq("JaneDoe"));
        verify(book).setId(Mockito.<Long>any());
        verify(book).setLibrary(Mockito.<Library>any());
        verify(book).setName(eq("Name"));
        verify(libraryRepository).findById(Mockito.<Long>any());
        verify(bookRepository).save(Mockito.<Book>any());
        assertEquals("JaneDoe", actualCreateResult.getAuthor());
        assertEquals("Name", actualCreateResult.getName());
        assertEquals(1L, actualCreateResult.getId().longValue());
    }

    /**
     * Method under test: {@link BookServiceImpl#create(BookRequest)}
     */
    @Test
    void testCreate4() {
        // Arrange
        Library library = new Library();
        library.setAddress("42 Main St");
        library.setBooks(new ArrayList<>());
        library.setId(1L);
        library.setName("Name");
        Book book = mock(Book.class);
        doNothing().when(book).setAuthor(Mockito.<String>any());
        doNothing().when(book).setId(Mockito.<Long>any());
        doNothing().when(book).setLibrary(Mockito.<Library>any());
        doNothing().when(book).setName(Mockito.<String>any());
        book.setAuthor("JaneDoe");
        book.setId(1L);
        book.setLibrary(library);
        book.setName("Name");
        Optional<Library> emptyResult = Optional.empty();
        when(libraryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
        BookRequest bookRequest = BookRequest.builder().author("JaneDoe").libraryId(1L).name("Name").build();

        // Act and Assert
        assertThrows(NotFoundException.class, () -> bookServiceImpl.create(bookRequest));
        verify(book).setAuthor(eq("JaneDoe"));
        verify(book).setId(Mockito.<Long>any());
        verify(book).setLibrary(Mockito.<Library>any());
        verify(book).setName(eq("Name"));
        verify(libraryRepository).findById(Mockito.<Long>any());
    }
}
